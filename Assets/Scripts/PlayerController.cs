﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public float jumpForce = 10;

    public Rigidbody rigidbody;
	public AudioSource flapSound;

	public UnityEvent onCheckpointHit;
	public UnityEvent onPipeHit;

    private bool jumpPressed = false;
	private bool allowMovement = true;

    // Update is called once per frame
    void Update()
    {
		if (!allowMovement)
			return;

		if (Input.GetKeyDown(KeyCode.Space))
			jumpPressed = true;
    }

    void FixedUpdate()
    {
		if (jumpPressed)
		{
			rigidbody.velocity = Vector3.up * jumpForce;
			jumpPressed = false;
			
			if (flapSound != null)
				flapSound.Play();
		}
    }

	void OnTriggerEnter(Collider col)
	{
		Debug.Log(col.gameObject.tag);
		if (col.gameObject.tag.Equals("Pipe"))
		{
			allowMovement = false;
			gameObject.layer = 2;
			GameManager.Instance.NotifyHit();
			
			if (onPipeHit != null)
			{
				onPipeHit.Invoke();
			}
		}
		else if (col.gameObject.tag.Equals("Checkpoint"))
		{
			if (onCheckpointHit != null)
				onCheckpointHit.Invoke();
		}
	}
}
