﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour
{
	public Text text;

	public void UpdateScore()
	{
		text.text = GameManager.Instance.score.ToString();
	}
}
