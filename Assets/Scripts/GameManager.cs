﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public PipeManager pipeManager;
	public PlayerController player;

	public int score;

	private static GameManager _instance;
	public static GameManager Instance { get { return _instance; } }

	void Awake()
	{
		if (_instance == null)
			_instance = this;
	}

	public void NotifyHit()
	{
		pipeManager.StopPipes();
	}

	public void IncrementScore()
	{
		score++;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.R))
		{
			SceneManager.LoadScene(0);
		}
	}
}
