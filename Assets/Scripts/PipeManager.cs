﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeManager : MonoBehaviour
{
    public PipeController pipePrefab;
	public float interval;
	public float scrollSpeed;

    public float[] zones;

    public float range;
    public int minZonePipeCount;
    public int maxZonePipeCount;
    public float pipeGap;

    public Renderer floor;

    private Pool<PipeController> pipePool;

    private int currentZone;
    private int currentMaxPipeCount;
    private int currentPipeCount;

    private bool allowSpawn = true;
    private float timer;

    // Use this for initialization
    void Start()
    {
		pipePool = PoolManager.CreatePool<PipeController>(pipePrefab, 10);

		StartCoroutine(SpawnCoroutine());
    }

    void Update()
    {
        if (allowSpawn)
        {
            //transform.Translate(Vector3.right * scrollSpeed * Time.deltaTime);
            floor.material.mainTextureOffset += Vector2.right / 6f * scrollSpeed * Time.deltaTime;
        }
    }

    private IEnumerator SpawnCoroutine()
    {
        while (allowSpawn)
        {
            SpawnPipes();
            yield return new WaitForSeconds(interval);
        }
    }

	private void SpawnPipes()
	{
		var pipe = pipePool.Spawn();
        pipe.transform.position = transform.position;
		pipe.speed = scrollSpeed;

        AssignPosition(pipe.transform);

        pipe.SetGap(pipeGap);
        pipe.StartScroll();
	}

    private void AssignPosition(Transform pipeTransform)
    {
        if (currentPipeCount <= 0)
        {
            currentZone = Random.Range(0, zones.Length);
            currentMaxPipeCount = Random.Range(minZonePipeCount, maxZonePipeCount + 1);
            currentPipeCount = currentMaxPipeCount;
        }

        var offset = Random.Range(-range, range);

        var position = pipeTransform.position;
        position.y = zones[currentZone] + offset;
        pipeTransform.position = position;

        currentPipeCount--;
    }

    public void StopPipes()
    {
        pipePool.ForEach(pipe => pipe.StopScroll());
        allowSpawn = false;
    }
}
