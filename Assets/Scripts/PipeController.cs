using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeController : MonoBehaviour
{
    public float speed;
    
    public Transform top;
    public Transform bottom;

    private bool allowScroll = false;

    void Update()
    {
        if (allowScroll)
            transform.Translate(-Vector3.right * speed * Time.deltaTime);
    }

    public void SetGap(float gap)
    {
        var halfGap = gap * 0.5f;
        top.localPosition = Vector3.up * (top.localScale.y + halfGap);
        bottom.localPosition = -Vector3.up * (bottom.localScale.y + halfGap);
    }

    public void StartScroll()
    {
        allowScroll = true;
    }

    public void StopScroll()
    {
        allowScroll = false;
    }
}