﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public static Pool<T> CreatePool<T>(T prefab, int initialAmount = 10, PoolType type = PoolType.Recycled) where T : Component
    {
        var root = new GameObject("Pool " + prefab.GetType());
        var comp = root.AddComponent<PoolManager>();
        var pool = new Pool<T>(comp, prefab, initialAmount, type);

        return pool;
    }
}

public class Pool<T> where T : Component
{
    public PoolManager pool { get; private set; }
    public PoolType type { get; set; }

    public T prefab { get; private set; }

    private int _allocationSize;
    public int allocationSize
    {
        get { return _allocationSize; }
        set { _allocationSize = Mathf.Max(value, 1); }
    }

    private List<T> poolList = new List<T>();
    private List<T> activePoolList = new List<T>();

    public Pool(PoolManager pool, T prefab, int allocationSize, PoolType type)
    {
        this.pool = pool;
        this.type = type;
        this.allocationSize = allocationSize;
        this.prefab = prefab;

        Reallocate();
    }

    public T Spawn()
    {
        T item;

        if (poolList.Count > 0)
        {
            item = poolList[0];
            poolList.RemoveAt(0);
        }
        else
        {
            switch (type)
            {
                case PoolType.Recycled:
                default:
                    item = activePoolList[0];
                    activePoolList.RemoveAt(0);
                    break;
                case PoolType.Limited:
                    Debug.LogError("Pool is set to Limited and is now empty.");
                    return null;
                case PoolType.Dynamic:
                    Reallocate();
                    item = poolList[0];
                    poolList.RemoveAt(0);
                    break;
            }
        }

        var listener = item as IPoolEventListener;

        if (listener != null)
        {
            listener.OnSpawn();
        }
        else
        {
            item.gameObject.SetActive(true);
        }

        activePoolList.Add(item);

        return item;
    }

    public void Despawn(T item)
    {
        if (activePoolList.Contains(item))
        {
            var listener = item as IPoolEventListener;

            if (listener != null)
            {
                listener.OnDespawn();
            }
            else
            {
                item.gameObject.SetActive(false);
            }

            activePoolList.Remove(item);
            poolList.Add(item);
        }
    }

    public void Add(T item)
    {
        poolList.Add(item);
    }

    public void ForEach(System.Action<T> callback, bool includeActive = true, bool includeInactive = false)
    {
        if (includeActive)
        {
            for (int i = 0, len = activePoolList.Count; i < len; i++)
            {
                callback(activePoolList[i]);
            }
        }

        if (includeInactive)
        {
            for (int i = 0, len = poolList.Count; i < len; i++)
            {
                callback(poolList[i]);
            }
        }
    }

    private void Reallocate()
    {
        for (int i = 0; i < allocationSize; i++)
        {
            var item = Object.Instantiate(prefab, pool.transform.position, pool.transform.rotation);
            item.gameObject.SetActive(false);
            item.transform.SetParent(pool.transform);

            Add(item);
        }
    }
}

public enum PoolType
{
    Recycled = 0,
    Limited = 1,
    Dynamic = 2
}

public interface IPoolEventListener
{
    void OnSpawn();
    void OnDespawn();
}